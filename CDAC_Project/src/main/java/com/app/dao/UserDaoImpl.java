package com.app.dao;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.User;

@Repository
public class UserDaoImpl implements IUserDao {

	private static Logger log = Logger.getLogger(UserDaoImpl.class.getName());

	@Autowired
	private EntityManager entityManager;

	@Override
	public long add(User pj) {
		log.info("UserDAOImpl Add method Start");
		Session session = entityManager.unwrap(Session.class);
		long pk = (long) session.save(pj);
		log.info("UserDAOImpl Add method End");
		return pk;
	}

	@Override
	public void delete(User pj) {
		log.info("UserDAOImpl Delete method Start");
		Session session = entityManager.unwrap(Session.class);
		session.delete(pj);
		log.info("UserDAOImpl Delete method End");

	}

	@Override
	public User findBypk(long pk) {
		log.info("UserDAOImpl FindByPk method Start");
		Session session = entityManager.unwrap(Session.class);
		User pj = (User) session.get(User.class, pk);
		log.info("UserDAOImpl FindByPk method End");
		return pj;
	}

	@Override
	public User findByLogin(String login) {
		log.info("UserDAOImpl FindByLogin method Start");
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(User.class);
		criteria.add(Restrictions.eq("login", login));
		User pj = (User) criteria.uniqueResult();
		log.info("UserDAOImpl FindByLogin method End");
		return pj;
	}

	@Override
	public void update(User entity) {
		log.info("UserDAOImpl Update method Start");
		Session session = entityManager.unwrap(Session.class);
		session.merge(entity);
		log.info("UserDAOImpl update method End");
	}

	@Override
	public List<User> list() {
		return list(0, 0);
	}

	@Override
	public List<User> list(int pageNo, int pageSize) {
		log.info("UserDAOImpl List method Start");
		Session session = entityManager.unwrap(Session.class);
		Query<User> query = session.createQuery("from User", User.class);
		List<User> list = query.getResultList();
		log.info("UserDAOImpl List method End");
		return list;
	}

	@Override
	public List<User> search(User pj) {
		return search(pj, 0, 0);
	}

	@Override
	public List<User> search(User pj, int pageNo, int pageSize) {
		log.info("UserDAOImpl Search method Start");
		Session session = entityManager.unwrap(Session.class);
		StringBuffer hql = new StringBuffer("from User as u where 1=1 ");
		if (pj != null) {
			if (pj.getId() > 0) {
				hql.append("and u.id = " + pj.getId());
			}
			if (pj.getRoleId() > 0) {
				hql.append("and u.roleId = " + pj.getRoleId());
			}
			if (pj.getFirstName() != null && pj.getFirstName().length() > 0) {
				hql.append("and u.firstName like '%" + pj.getFirstName() + "%'");
			}
			if (pj.getEmail() != null && pj.getEmail().length() > 0) {
				hql.append("and u.email like '%" + pj.getEmail() + "%'");
			}
		}
		Query<User> query = session.createQuery(hql.toString(), User.class);
		if (pageNo > 0) {
			pageNo = (pageNo - 1) * pageSize;
			query.setFirstResult(pageNo);
			query.setMaxResults(pageSize);
		}
		List<User> list = query.getResultList();
		log.info("UserDAOImpl Search method End");
		return list;
	}

	@Override
	public User authentication(User pj) {
		log.info("UserDAOImpl Authentication method Start");
		Session session = entityManager.unwrap(Session.class);
		Query<User> query = session.createQuery("from User where login=:login and password=:password",
				User.class);
		query.setParameter("login", pj.getLogin());
		query.setParameter("password", pj.getPassword());
		pj = null;
		try {
			pj = query.getSingleResult();
		} catch (NoResultException nre) {
		}
		log.info("UserDAOImpl Authentication method End");
		return pj;
	}

	/*
	 * @Override public User authentication(User pj) {
	 * log.info("UserDAOImpl Authentication method Start"); Session session =
	 * entityManager.unwrap(Session.class); String
	 * hql="from User as u where u.login='"+pj.getLogin()+"' and u.password='"
	 * +pj.getPassword()+"'"; System.out.println(hql); Query<User> query =
	 * session .createQuery(hql, User.class); pj = null; try { pj =
	 * query.getSingleResult(); } catch (NoResultException nre) { }
	 * log.info("UserDAOImpl Authentication method End"); return pj; }
	 */

	@Override
	public Blob getImageById(long id) throws SerialException, SQLException {

		Session session = entityManager.unwrap(Session.class);
		User user = (User) session.get(User.class, id);
		byte[] blob = user.getProfilePic();
		Blob bBlob = new SerialBlob(blob);
		return bBlob;
	}

}
