package com.app.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Exprience;



@Repository
public class ExprienceDAOImpl implements IExprienceDao {

	private static Logger log = Logger.getLogger(ExprienceDAOImpl.class.getName());

	@Autowired
	private EntityManager entityManager;

	@Override
	public long add(Exprience pj) {
		log.info("ExprienceDAOImpl Add method Start");
		Session session = entityManager.unwrap(Session.class);
		long pk = (long) session.save(pj);
		log.info("ExprienceDAOImpl Add method End");
		return pk;
	}

	@Override
	public void delete(Exprience dto) {
		log.info("ExprienceDAOImpl Delete method Start");
		Session session = entityManager.unwrap(Session.class);
		session.delete(dto);
		log.info("ExprienceDAOImpl Delete method End");

	}

	@Override
	public Exprience findBypk(long pk) {
		log.info("ExprienceDAOImpl FindByPk method Start");
		Session session = entityManager.unwrap(Session.class);
		Exprience pj = (Exprience) session.get(Exprience.class, pk);
		log.info("ExprienceDAOImpl FindByPk method End");
		return pj;
	}

	@Override
	public Exprience findByName(String name) {
		log.info("ExprienceDAOImpl FindByLogin method Start");
		Session session = entityManager.unwrap(Session.class);
		@SuppressWarnings("deprecation")
		Criteria criteria = session.createCriteria(Exprience.class);
		criteria.add(Restrictions.eq("name", name));
		Exprience dto = (Exprience) criteria.uniqueResult();
		log.info("ExprienceDAOImpl FindByLogin method End");
		return dto;
	}

	@Override
	public void update(Exprience entity) {
		log.info("ExprienceDAOImpl Update method Start");
		Session session = entityManager.unwrap(Session.class);
		session.merge(entity);
		log.info("ExprienceDAOImpl update method End");
	}

	@Override
	public List<Exprience> list() {
		return list(0, 0);
	}

	@Override
	public List<Exprience> list(int pageNo, int pageSize) {
		log.info("ExprienceDAOImpl List method Start");
		Session session = entityManager.unwrap(Session.class);
		Query<Exprience> query = session.createQuery("from Exprience", Exprience.class);
		List<Exprience> list = query.getResultList();
		log.info("ExprienceDAOImpl List method End");
		return list;
	}

	@Override
	public List<Exprience> search(Exprience dto) {
		return search(dto, 0, 0);
	}

	@Override
	public List<Exprience> search(Exprience dto, int pageNo, int pageSize) {
		log.info("ExprienceDAOImpl Search method Start");
		Session session = entityManager.unwrap(Session.class);
		StringBuffer hql=new StringBuffer("from Exprience as e where 1=1 ");
		if(dto !=null) {
			if(dto.getId()>0) {
				hql.append("and e.id = "+dto.getId());
			}
			if(dto.getApplicationId()>0) {
				hql.append("and e.applicationId = "+dto.getApplicationId());
			}
		}
		System.out.println(hql.toString());
		Query<Exprience> query = session.createQuery(hql.toString(), Exprience.class);
		List<Exprience> list = query.getResultList();
		log.info("ExprienceDAOImpl Search method End");
		return list;
	}
	
	

}
