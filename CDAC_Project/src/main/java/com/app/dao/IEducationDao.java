package com.app.dao;
import com.app.pojos.Education;
import java.util.List;




public interface IEducationDao {

	public long add(Education pj);
	
	public void delete(Education pj);
	
	public Education findBypk(long pk);
	
	public Education findByName(String name);
	
	public void update(Education pj);
	
	public List<Education> list();
	
	public List<Education>list(int pageNo,int pageSize);
	
	public List<Education> search(Education pj);
	
	public List<Education> search(Education pj,int pageNo,int pageSize);
	
}
