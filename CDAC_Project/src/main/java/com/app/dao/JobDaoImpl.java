package com.app.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Job;

@Repository
public class JobDaoImpl implements IJobDao {

	private static Logger log = Logger.getLogger(JobDaoImpl.class.getName());

	@Autowired
	private EntityManager entityManager;

	@Override
	public long add(Job pj) {
		log.info("JobDAOImpl Add method Start");
		Session session = entityManager.unwrap(Session.class);
		long pk = (long) session.save(pj);
		log.info("JobDAOImpl Add method End");
		return pk;
	}

	@Override
	public void delete(Job pj) {
		log.info("JobDAOImpl Delete method Start");
		Session session = entityManager.unwrap(Session.class);
		session.delete(pj);
		log.info("JobDAOImpl Delete method End");

	}

	@Override
	public Job findBypk(long pk) {
		log.info("JobDAOImpl FindByPk method Start");
		Session session = entityManager.unwrap(Session.class);
		Job pj = (Job) session.get(Job.class, pk);
		log.info("JobDAOImpl FindByPk method End");
		return pj;
	}

	@Override
	public Job findByName(String name) {
		log.info("JobDAOImpl FindByLogin method Start");
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Job.class);
		criteria.add(Restrictions.eq("name", name));
		Job pj = (Job) criteria.uniqueResult();
		log.info("JobDAOImpl FindByLogin method End");
		return pj;
	}

	@Override
	public void update(Job entity) {
		log.info("JobDAOImpl Update method Start");
		Session session = entityManager.unwrap(Session.class);
		session.merge(entity);
		log.info("JobDAOImpl update method End");
	}

	@Override
	public List<Job> list() {
		return list(0, 0);
	}

	@Override
	public List<Job> list(int pageNo, int pageSize) {
		log.info("JobDAOImpl List method Start");
		Session session = entityManager.unwrap(Session.class);
		Query<Job> query = session.createQuery("from Job", Job.class);
		List<Job> list = query.getResultList();
		log.info("JobDAOImpl List method End");
		return list;
	}

	@Override
	public List<Job> search(Job pj) {
		return search(pj, 0, 0);
	}

	@Override
	public List<Job> search(Job pj, int pageNo, int pageSize) {
		log.info("JobDAOImpl Search method Start");
		Session session = entityManager.unwrap(Session.class);
		StringBuffer hql=new StringBuffer("from Job as j where 1=1 ");
		if(pj !=null) {
			if(pj.getId()>0) {
				hql.append("and j.id = "+pj.getId());
			}
			if(pj.getRecruiterId()>0) {
				hql.append("and j.recruiterId = "+pj.getRecruiterId());
			}
			if(pj.getName()!=null && pj.getName().length()>0) {
				hql.append("and j.name like '%"+pj.getName()+"%'");
			}
			if(pj.getCompanyName()!=null && pj.getCompanyName().length()>0) {
				hql.append("and j.companyName like '%"+pj.getCompanyName()+"%'");
			}
			if(pj.getLanguage()!=null && pj.getLanguage().length()>0) {
				hql.append("and j.language like '%"+pj.getLanguage()+"%'");
			}
		}
		Query<Job> query = session.createQuery(hql.toString(), Job.class);
		if (pageNo > 0) {
			pageNo = (pageNo - 1) * pageSize;
			query.setFirstResult(pageNo);
			query.setMaxResults(pageSize);
		}
		List<Job> list = query.getResultList();
		log.info("JobDAOImpl Search method End");
		return list;
	}

}
