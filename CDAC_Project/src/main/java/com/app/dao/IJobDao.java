package com.app.dao;

import java.util.List;

import com.app.pojos.Job;



public interface IJobDao {

	public long add(Job pj);
	
	public void delete(Job pj);
	
	public Job findBypk(long pk);
	
	public Job findByName(String name);
	
	public void update(Job pj);
	
	public List<Job> list();
	
	public List<Job>list(int pageNo,int pageSize);
	
	public List<Job> search(Job pj);
	
	public List<Job> search(Job pj,int pageNo,int pageSize);
	
}
