package com.app.dao;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.sql.rowset.serial.SerialBlob;
import javax.sql.rowset.serial.SerialException;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.ApplyJob;
import com.app.pojos.User;



@Repository
public class ApplyJobDaoImpl implements IApplyJobDao {

	private static Logger log = Logger.getLogger(ApplyJobDaoImpl.class.getName());

	@Autowired
	private EntityManager entityManager;

	@Override
	public long add(ApplyJob pj) {
		log.info("ApplyJobDaoImpl Add method Start");
		Session session = entityManager.unwrap(Session.class);
		long pk = (long) session.save(pj);
		log.info("ApplyJobDaoImpl Add method End");
		return pk;
	}

	@Override
	public void delete(ApplyJob pj) {
		log.info("ApplyJobDAOImpl Delete method Start");
		Session session = entityManager.unwrap(Session.class);
		session.delete(pj);
		log.info("ApplyJobDAOImpl Delete method End");

	}

	@Override
	public ApplyJob findBypk(long pk) {
		log.info("ApplyJobDaoImpl FindByPk method Start");
		Session session = entityManager.unwrap(Session.class);
		ApplyJob pj = (ApplyJob) session.get(ApplyJob.class, pk);
		log.info("ApplyJobDaoImpl FindByPk method End");
		return pj;
	}

	@Override
	public ApplyJob findByName(String name) {
		log.info("ApplyJobDaoImpl FindByLogin method Start");
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(ApplyJob.class);
		criteria.add(Restrictions.eq("name", name));
		ApplyJob pj = (ApplyJob) criteria.uniqueResult();
		log.info("ApplyJobDaoImpl FindByLogin method End");
		return pj;
	}

	@Override
	public void update(ApplyJob entity) {
		log.info("ApplyJobDaoImpl Update method Start");
		Session session = entityManager.unwrap(Session.class);
		session.merge(entity);
		log.info("ApplyJobDaoImpl update method End");
	}

	@Override
	public List<ApplyJob> list() {
		return list(0, 0);
	}

	@Override
	public List<ApplyJob> list(int pageNo, int pageSize) {
		log.info("ApplyJobDaoImpl List method Start");
		Session session = entityManager.unwrap(Session.class);
		Query<ApplyJob> query = session.createQuery("from ApplyJob", ApplyJob.class);
		List<ApplyJob> list = query.getResultList();
		log.info("ApplyJobDaoImpl List method End");
		return list;
	}

	@Override
	public List<ApplyJob> search(ApplyJob pj) {
		return search(pj, 0, 0);
	}

	@Override
	public List<ApplyJob> search(ApplyJob pj, int pageNo, int pageSize) {
		log.info("ApplyJobDaoImpl Search method Start");
		Session session = entityManager.unwrap(Session.class);
		
		StringBuffer hql=new StringBuffer("from ApplyJob as a where 1=1 ");
		if(pj !=null) {
			if(pj.getId()>0) {
				hql.append("and a.id = "+pj.getId());
			}
			if(pj.getRecruiterId()>0) {
				hql.append("and a.recruiterId = "+pj.getRecruiterId());
			}
			if(pj.getUserId()>0) {
				hql.append("and a.userId = "+pj.getUserId());
			}
			if(pj.getUserName()!=null && pj.getUserName().length()>0) {
				hql.append("and a.userName like '%"+pj.getUserName()+"%'");
			}
			if(pj.getFileName()!=null && pj.getFileName().length()>0) {
				hql.append("and a.fileName like '%"+pj.getFileName()+"%'");
			}
		}
		Query<ApplyJob> query = session.createQuery(hql.toString(),ApplyJob.class);
		if (pageNo > 0) {
			pageNo = (pageNo - 1) * pageSize;
			query.setFirstResult(pageNo);
			query.setMaxResults(pageSize);
		}
		List<ApplyJob> list = query.getResultList();
		log.info("ApplyJobDaoImpl Search method End");
		return list;
	}
	
	@Override
	public Blob getFileById(long id) throws SerialException, SQLException {
		Session session = entityManager.unwrap(Session.class);
		ApplyJob user = (ApplyJob) session.get(ApplyJob.class, id);
        byte[] blob = user.getResumeFile();
        Blob bBlob= new SerialBlob(blob);
		return bBlob;
	}

}
