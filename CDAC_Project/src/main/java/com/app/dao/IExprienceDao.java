package com.app.dao;

import java.util.List;

import com.app.pojos.Exprience;

public interface IExprienceDao {

	public long add(Exprience pj);
	
	public void delete(Exprience pj);
	
	public Exprience findBypk(long pk);
	
	public Exprience findByName(String name);
	
	public void update(Exprience pj);
	
	public List<Exprience> list();
	
	public List<Exprience>list(int pageNo,int pageSize);
	
	public List<Exprience> search(Exprience pj);
	
	public List<Exprience> search(Exprience pj,int pageNo,int pageSize);
	
}
