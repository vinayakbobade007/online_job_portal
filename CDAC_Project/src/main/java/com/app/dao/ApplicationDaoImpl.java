package com.app.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Application;


@Repository
public class ApplicationDaoImpl implements IApplicationDao {

	private static Logger log = Logger.getLogger(IApplicationDao.class.getName());

	@Autowired
	private EntityManager entityManager;

	@Override
	public long add(Application pj) {
		log.info("ApplicationDAOImpl Add method Start");
		Session session = entityManager.unwrap(Session.class);
		long pk = (long) session.save(pj);
		log.info("IApplicationDao Add method End");
		return pk;
	}

	@Override
	public void delete(Application pj) {
		log.info("ApplicationDaoImpl Delete method Start");
		Session session = entityManager.unwrap(Session.class);
		session.delete(pj);
		log.info("ApplicationDaoImpl Delete method End");

	}

	@Override
	public Application findBypk(long pk) {
		log.info("ApplicationDaoImpl FindByPk method Start");
		Session session = entityManager.unwrap(Session.class);
		Application pj = (Application) session.get(Application.class, pk);
		log.info("ApplicationDaoImpl FindByPk method End");
		return pj;
	}

	@Override
	public Application findByName(String name) {
		log.info("ApplicationDaoImpl FindByLogin method Start");
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Application.class);
		criteria.add(Restrictions.eq("name", name));
		Application pj = (Application) criteria.uniqueResult();
		log.info("ApplicationDaoImpl FindByLogin method End");
		return pj;
	}
	
	@Override
	public Application findByUserId(long id) {
		log.info("ApplicationDaoImpl FindByLogin method Start");
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Application.class);
		criteria.add(Restrictions.eq("userId", id));
		Application pj = (Application) criteria.uniqueResult();
		log.info("ApplicationDaoImpl FindByLogin method End");
		return pj;
	}

	@Override
	public void update(Application entity) {
		log.info("ApplicationDaoImpl Update method Start");
		Session session = entityManager.unwrap(Session.class);
		session.merge(entity);
		log.info("ApplicationDaoImpl update method End");
	}

	@Override
	public List<Application> list() {
		return list(0, 0);
	}

	@Override
	public List<Application> list(int pageNo, int pageSize) {
		log.info("ApplicationDaoImpl List method Start");
		Session session = entityManager.unwrap(Session.class);
		Query<Application> query = session.createQuery("from Application", Application.class);
		List<Application> list = query.getResultList();
		log.info("ApplicationDAOImpl List method End");
		return list;
	}

	@Override
	public List<Application> search(Application pj) {
		return search(pj, 0, 0);
	}

	@Override
	public List<Application> search(Application pj, int pageNo, int pageSize) {
		log.info("ApplicationDaoImpl Search method Start");
		Session session = entityManager.unwrap(Session.class);
		Query<Application> query = session.createQuery("from Application", Application.class);
		List<Application> list = query.getResultList();
		log.info("ApplicationDaoImpl Search method End");
		return list;
	}
	
	

}
