package com.app.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.app.pojos.Education;


@Repository
public class EducationDaoImpl implements IEducationDao {

	private static Logger log = Logger.getLogger(EducationDaoImpl.class.getName());

	@Autowired
	private EntityManager entityManager;

	@Override
	public long add(Education pj) {
		log.info("EducationDAOImpl Add method Start");
		Session session = entityManager.unwrap(Session.class);
		long pk = (long) session.save(pj);
		log.info("EducationDAOImpl Add method End");
		return pk;
	}

	@Override
	public void delete(Education pj) {
		log.info("EducationDAOImpl Delete method Start");
		Session session = entityManager.unwrap(Session.class);
		session.delete(pj);
		log.info("EducationDAOImpl Delete method End");

	}

	@Override
	public Education findBypk(long pk) {
		log.info("EducationDAOImpl FindByPk method Start");
		Session session = entityManager.unwrap(Session.class);
		Education pj = (Education) session.get(Education.class, pk);
		log.info("EducationDAOImpl FindByPk method End");
		return pj;
	}

	@Override
	public Education findByName(String name) {
		log.info("EducationDAOImpl FindByLogin method Start");
		Session session = entityManager.unwrap(Session.class);
		Criteria criteria = session.createCriteria(Education.class);
		criteria.add(Restrictions.eq("name", name));
		Education pj = (Education) criteria.uniqueResult();
		log.info("EducationDAOImpl FindByLogin method End");
		return pj;
	}

	@Override
	public void update(Education entity) {
		log.info("EducationDAOImpl Update method Start");
		Session session = entityManager.unwrap(Session.class);
		session.merge(entity);
		log.info("EducationDAOImpl update method End");
	}

	@Override
	public List<Education> list() {
		return list(0, 0);
	}

	@Override
	public List<Education> list(int pageNo, int pageSize) {
		log.info("EducationDAOImpl List method Start");
		Session session = entityManager.unwrap(Session.class);
		Query<Education> query = session.createQuery("from Education", Education.class);
		List<Education> list = query.getResultList();
		log.info("EducationDAOImpl List method End");
		return list;
	}

	@Override
	public List<Education> search(Education pj) {
		return search(pj, 0, 0);
	}

	@Override
	public List<Education> search(Education pj, int pageNo, int pageSize) {
		log.info("EducationDAOImpl Search method Start");
		Session session = entityManager.unwrap(Session.class);
		StringBuffer hql=new StringBuffer("from Education as e where 1=1 ");
		if(pj !=null) {
			if(pj.getId()>0) {
				hql.append("and e.id = "+pj.getId());
			}
			if(pj.getApplicationId()>0) {
				hql.append("and e.applicationId = "+pj.getApplicationId());
			}
		}
		System.out.println(hql.toString());
		Query<Education> query = session.createQuery(hql.toString(), Education.class);
	
		List<Education> list = query.getResultList();
		log.info("EducationDAOImpl Search method End");
		return list;
	}
	
	

}
