package com.app.dao;

import java.util.List;

import com.app.pojos.Application;


public interface IApplicationDao {

	public long add(Application pj);
	
	public void delete(Application pj);
	
	public Application findBypk(long pk);
	
	public Application findByUserId(long pk);
	
	public Application findByName(String name);
	
	public void update(Application pj);
	
	public List<Application> list();
	
	public List<Application>list(int pageNo,int pageSize);
	
	public List<Application> search(Application pj);
	
	public List<Application> search(Application pj,int pageNo,int pageSize);
	
}
