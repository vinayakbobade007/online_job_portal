package com.app.service;
import java.util.List;

import com.app.exception.DuplicateRecordException;
import com.app.pojos.Education;

public interface IEducationService {

	public long add(Education pj) throws DuplicateRecordException;

	public void delete(Education pj);

	public Education findBypk(long pk);

	public Education findByName(String name);

	public void update(Education pj) throws DuplicateRecordException;

	public List<Education> list();

	public List<Education> list(int pageNo, int pageSize);

	public List<Education> search(Education pj);

	public List<Education> search(Education pj, int pageNo, int pageSize);
	




}
