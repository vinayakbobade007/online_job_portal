package com.app.service;



import java.util.List;

import com.app.exception.DuplicateRecordException;
import com.app.pojos.Exprience;





public interface IExprienceService {

	public long add(Exprience pj) throws DuplicateRecordException;

	public void delete(Exprience pj);

	public Exprience findBypk(long pk);

	public Exprience findByName(String name);

	public void update(Exprience pj) throws DuplicateRecordException;

	public List<Exprience> list();

	public List<Exprience> list(int pageNo, int pageSize);

	public List<Exprience> search(Exprience pj);

	public List<Exprience> search(Exprience pj, int pageNo, int pageSize);
	




}
