package com.app.service;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import javax.sql.rowset.serial.SerialException;

import com.app.exception.DuplicateRecordException;
import com.app.pojos.User;

public interface IUserService {

	public long add(User pj) throws DuplicateRecordException;

	public void delete(User pj);

	public User findBypk(long pk);

	public User findByLogin(String login);

	public void update(User pj) throws DuplicateRecordException;

	public List<User> list();

	public List<User> list(int pageNo, int pageSize);

	public List<User> search(User pj);

	public List<User> search(User pj, int pageNo, int pageSize);

	public User authentication(User pj);

	public boolean changePassword(Long id, String oldPassword, String newPassword);

	public boolean forgetPassword(String login);
	
	public Blob getImageById(long id) throws SerialException, SQLException;

}
