package com.app.service;

import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IJobDao;
import com.app.pojos.Job;
import com.app.exception.DuplicateRecordException;
import com.app.util.EmailBuilder;



@Service
public class JobServiceImpl implements IJobService {

	private static Logger log=Logger.getLogger(JobServiceImpl.class.getName());
	
	@Autowired
	private IJobDao dao;
	
	
	
	@Override
	@Transactional
	public long add(Job pj) throws DuplicateRecordException {
		log.info("JobServiceImpl Add method start");
		Job existpj=dao.findByName(pj.getName());
		if(existpj !=null)
			throw new DuplicateRecordException("Job Already Exist");
		long pk=dao.add(pj);
		log.info("JobServiceImpl Add method end");
		return pk;
	}

	@Override
	@Transactional
	public void delete(Job pj) {
		log.info("JobServiceImpl Delete method start");
		dao.delete(pj);
		log.info("JobServiceImpl Delete method end");
		
	}

	@Override
	@Transactional
	public Job findBypk(long pk) {
		log.info("JobServiceImpl findBypk method start");
		Job pj=dao.findBypk(pk);
		log.info("JobServiceImpl findBypk method end");
		return pj;
	}

	@Override
	@Transactional
	public Job findByName(String name) {
		log.info("JobServiceImpl findByJobName method start");
		Job pj=dao.findByName(name);
		log.info("JobServiceImpl findByJobName method end");
		return pj;
	}

	@Override
	@Transactional
	public void update(Job pj) throws DuplicateRecordException {
		log.info("JobServiceImpl update method start");
		Job existpj=dao.findByName(pj.getName());
		if(existpj !=null && pj.getId()!=existpj.getId())
			throw new DuplicateRecordException("Name Already Exist");
		dao.update(pj);
		log.info("JobServiceImpl update method end");
	}

	@Override
	@Transactional
	public List<Job> list() {
		log.info("JobServiceImpl list method start");
		List<Job> list=dao.list();
		log.info("JobServiceImpl list method end");
		return list;
	}

	@Override
	@Transactional
	public List<Job> list(int pageNo, int pageSize) {
		log.info("JobServiceImpl list method start");
		List<Job> list=dao.list(pageNo, pageSize);
		log.info("JobServiceImpl list method end");
		return list;
	}

	@Override
	@Transactional
	public List<Job> search(Job pj) {
		log.info("JobServiceImpl search method start");
		List<Job> list=dao.search(pj);
		log.info("JobServiceImpl search method end");
		return list;
	}

	@Override
	@Transactional
	public List<Job> search(Job pj, int pageNo, int pageSize) {
		log.info("JobServiceImpl search method start");
		List<Job> list=dao.search(pj, pageNo, pageSize);
		log.info("JobServiceImpl search method end");
		return list;
	}

	
	
	
	
	
}
