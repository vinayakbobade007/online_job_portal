package com.app.service;


import java.util.List;
import java.util.logging.Logger;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IExprienceDao;
import com.app.pojos.Exprience;


import com.app.exception.DuplicateRecordException;


@Service
public class ExprienceServiceImpl implements IExprienceService {

	private static Logger log=Logger.getLogger(ExprienceServiceImpl.class.getName());
	
	@Autowired
	private IExprienceDao dao;
	
	
	
	@Transactional
	public long add(Exprience pj) throws DuplicateRecordException {
		log.info("ExprienceServiceImpl Add method start");
		
		long pk=dao.add(pj);
		log.info("ExprienceServiceImpl Add method end");
		return pk;
	}

	@Transactional
	public void delete(Exprience dto) {
		log.info("ExprienceServiceImpl Delete method start");
		dao.delete(dto);
		log.info("ExprienceServiceImpl Delete method end");
		
	}

	@Override
	@Transactional
	public Exprience findBypk(long pk) {
		log.info("ExprienceServiceImpl findBypk method start");
		Exprience pj=dao.findBypk(pk);
		log.info("ExprienceServiceImpl findBypk method end");
		return pj;
	}

	@Override
	@Transactional
	public Exprience findByName(String name) {
		log.info("ExprienceServiceImpl findByExprienceName method start");
		Exprience pj=dao.findByName(name);
		log.info("ExprienceServiceImpl findByExprienceName method end");
		return pj;
	}

	@Override
	@Transactional
	public void update(Exprience pj) throws DuplicateRecordException {
		log.info("ExprienceServiceImpl update method start");
		dao.update(pj);
		log.info("ExprienceServiceImpl update method end");
	}

	@Override
	@Transactional
	public List<Exprience> list() {
		log.info("ExprienceServiceImpl list method start");
		List<Exprience> list=dao.list();
		log.info("ExprienceServiceImpl list method end");
		return list;
	}

	@Override
	@Transactional
	public List<Exprience> list(int pageNo, int pageSize) {
		log.info("ExprienceServiceImpl list method start");
		List<Exprience> list=dao.list(pageNo, pageSize);
		log.info("ExprienceServiceImpl list method end");
		return list;
	}

	@Override
	@Transactional
	public List<Exprience> search(Exprience pj) {
		log.info("ExprienceServiceImpl search method start");
		List<Exprience> list=dao.search(pj);
		log.info("ExprienceServiceImpl search method end");
		return list;
	}

	@Override
	@Transactional
	public List<Exprience> search(Exprience pj, int pageNo, int pageSize) {
		log.info("ExprienceServiceImpl search method start");
		List<Exprience> list=dao.search(pj, pageNo, pageSize);
		log.info("ExprienceServiceImpl search method end");
		return list;
	}

	
	
}
