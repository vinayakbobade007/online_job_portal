package com.app.service;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;

import javax.sql.rowset.serial.SerialException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IApplyJobDao;
import com.app.pojos.ApplyJob;
import com.app.exception.DuplicateRecordException;





@Service
public class ApplyJobServiceImpl implements IApplyJobService {

	private static Logger log=Logger.getLogger(ApplyJobServiceImpl.class.getName());
	
	@Autowired
	private IApplyJobDao dao;
	
	
	
	@Override
	@Transactional
	public long add(ApplyJob pj) throws DuplicateRecordException {
		log.info("ApplyJobServiceImpl Add method start");
		
		long pk=dao.add(pj);
		log.info("ApplyJobServiceImpl Add method end");
		return pk;
	}

	@Override
	@Transactional
	public void delete(ApplyJob pj) {
		log.info("ApplyJobServiceImpl Delete method start");
		dao.delete(pj);
		log.info("ApplyJobServiceImpl Delete method end");
		
	}

	@Override
	@Transactional
	public ApplyJob findBypk(long pk) {
		log.info("ApplyJobServiceImpl findBypk method start");
		ApplyJob pj=dao.findBypk(pk);
		log.info("ApplyJobServiceImpl findBypk method end");
		return pj;
	}

	@Override
	@Transactional
	public ApplyJob findByName(String name) {
		log.info("ApplyJobServiceImpl findByApplyJobName method start");
		ApplyJob pj=dao.findByName(name);
		log.info("ApplyJobServiceImpl findByApplyJobName method end");
		return pj;
	}

	@Override
	@Transactional
	public void update(ApplyJob pj) throws DuplicateRecordException {
		log.info("ApplyJobServiceImpl update method start");
		dao.update(pj);
		log.info("ApplyJobServiceImpl update method end");
	}

	@Override
	@Transactional
	public List<ApplyJob> list() {
		log.info("ApplyJobServiceImpl list method start");
		List<ApplyJob> list=dao.list();
		log.info("ApplyJobServiceImpl list method end");
		return list;
	}

	@Override
	@Transactional
	public List<ApplyJob> list(int pageNo, int pageSize) {
		log.info("ApplyJobServiceImpl list method start");
		List<ApplyJob> list=dao.list(pageNo, pageSize);
		log.info("ApplyJobServiceImpl list method end");
		return list;
	}

	@Override
	@Transactional
	public List<ApplyJob> search(ApplyJob pj) {
		log.info("ApplyJobServiceImpl search method start");
		List<ApplyJob> list=dao.search(pj);
		log.info("ApplyJobServiceImpl search method end");
		return list;
	}

	@Override
	@Transactional
	public List<ApplyJob> search(ApplyJob pj, int pageNo, int pageSize) {
		log.info("ApplyJobServiceImpl search method start");
		List<ApplyJob> list=dao.search(pj, pageNo, pageSize);
		log.info("ApplyJobServiceImpl search method end");
		return list;
	}

	@Override
	@Transactional
	public Blob getFileById(long id) throws SerialException, SQLException {
		return dao.getFileById(id);
	}

	
	
	
	
	
}
