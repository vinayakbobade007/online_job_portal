package com.app.service;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import javax.sql.rowset.serial.SerialException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IUserDao;
import com.app.exception.DuplicateRecordException;
import com.app.pojos.User;

import com.app.util.EmailBuilder;



@Service
@Transactional
public class UserServiceImpl implements IUserService {

	private static Logger log=Logger.getLogger(UserServiceImpl.class.getName());
	
	@Autowired
	private IUserDao dao;
	
	@Autowired
	private JavaMailSenderImpl mailSender;
	
	@Override
	public long add(User pj) throws DuplicateRecordException {
		log.info("UserServiceImpl Add method start");
		User existpj=dao.findByLogin(pj.getLogin());
		System.out.println("in Student add After finad b pk");
		if(existpj !=null)
			throw new DuplicateRecordException("Login Already Exist");
		long pk=dao.add(pj);
		log.info("UserServiceImpl Add method end");
		return pk;
	}

	@Override
	public void delete(User pj) {
		log.info("UserServiceImpl Delete method start");
		dao.delete(pj);
		log.info("UserServiceImpl Delete method end");
		
	}

	@Override
	public User findBypk(long pk) {
		log.info("UserServiceImpl findBypk method start");
		User pj=dao.findBypk(pk);
		log.info("UserServiceImpl findBypk method end");
		return pj;
	}

	@Override
	public User findByLogin(String login) {
		log.info("UserServiceImpl findByUserName method start");
		User pj=dao.findByLogin(login);
		log.info("UserServiceImpl findByUserName method end");
		return pj;
	}

	@Override
	public void update(User pj) throws DuplicateRecordException {
		log.info("UserServiceImpl update method start");
		User existpj=dao.findByLogin(pj.getLogin());
		if(existpj !=null && pj.getId()!=existpj.getId())
			throw new DuplicateRecordException("Login Already Exist");
		dao.update(pj);
		log.info("UserServiceImpl update method end");
	}

	@Override
	public List<User> list() {
		log.info("UserServiceImpl list method start");
		List<User> list=dao.list();
		log.info("UserServiceImpl list method end");
		return list;
	}

	@Override
	public List<User> list(int pageNo, int pageSize) {
		log.info("UserServiceImpl list method start");
		List<User> list=dao.list(pageNo, pageSize);
		log.info("UserServiceImpl list method end");
		return list;
	}

	@Override
	public List<User> search(User pj) {
		log.info("UserServiceImpl search method start");
		List<User> list=dao.search(pj);
		log.info("UserServiceImpl search method end");
		return list;
	}

	@Override
	public List<User> search(User pj, int pageNo, int pageSize) {
		log.info("UserServiceImpl search method start");
		List<User> list=dao.search(pj, pageNo, pageSize);
		log.info("UserServiceImpl search method end");
		return list;
	}

	@Override
	public User authentication(User pj) {
		log.info("UserServiceImpl authentication method start");
		pj=dao.authentication(pj);
		log.info("UserServiceImpl authentication method end");
		return pj;
	}
	
	@Override
	public boolean changePassword(Long id, String oldPassword, String newPassword) { 
		log.info("UserServiceImpl  changePassword method start");
		User pjExist = findBypk(id);
		if (pjExist != null && pjExist.getPassword().equals(oldPassword)) {
			pjExist.setPassword(newPassword);
			dao.update(pjExist);
			log.info("UserServiceImpl  changePassword method end");
			return true;
		} else {
			return false;
		}
	}
	
	@Transactional(propagation = Propagation.REQUIRED, readOnly = false)
	public boolean forgetPassword(String login) {         

		User pjExist = dao.findByLogin(login);

		if (pjExist != null) {

			HashMap<String, String> map = new HashMap<String, String>();
			map.put("firstName", pjExist.getFirstName());
			map.put("lastName", pjExist.getLastName());
			map.put("login", pjExist.getLogin());
			map.put("password", pjExist.getPassword());
			String message = EmailBuilder.getForgetPasswordMessage(map);
			MimeMessage msg = mailSender.createMimeMessage();
			try {
				MimeMessageHelper helper = new MimeMessageHelper(msg);
				helper.setTo(pjExist.getLogin());
				helper.setSubject("Login Regisration Password Password");
				helper.setText(message, true);
				mailSender.send(msg);
			} catch (MessagingException e) {
				e.printStackTrace();
				return false;
			}
		} else {
			return false;
		}
		return true;
	}
	
	@Override
	public Blob getImageById(long id) throws SerialException, SQLException {
		return dao.getImageById(id);
	}

	
}

