package com.app.service;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import javax.sql.rowset.serial.SerialException;

import com.app.pojos.Application;
import com.app.exception.DuplicateRecordException;

public interface IApplicationService {

	public long add(Application pj) throws DuplicateRecordException;

	public void delete(Application pj);

	public Application findBypk(long pk);

	public Application findByName(String name);
	
	public Application findByUserId(long id);

	public void update(Application pj) throws DuplicateRecordException;

	public List<Application> list();

	public List<Application> list(int pageNo, int pageSize);

	public List<Application> search(Application pj);

	public List<Application> search(Application pj, int pageNo, int pageSize);
	




}
