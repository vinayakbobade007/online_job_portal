package com.app.service;

import java.sql.Blob;
import java.sql.SQLException;
import java.util.List;

import javax.sql.rowset.serial.SerialException;

import com.app.pojos.ApplyJob;
import com.app.exception.DuplicateRecordException;

public interface IApplyJobService {

	public long add(ApplyJob pj) throws DuplicateRecordException;

	public void delete(ApplyJob pj);

	public ApplyJob findBypk(long pk);

	public ApplyJob findByName(String name);

	public void update(ApplyJob pj) throws DuplicateRecordException;

	public List<ApplyJob> list();

	public List<ApplyJob> list(int pageNo, int pageSize);

	public List<ApplyJob> search(ApplyJob pj);

	public List<ApplyJob> search(ApplyJob pj, int pageNo, int pageSize);
	
	public Blob getFileById(long id) throws SerialException, SQLException;




}
