package com.app.service;

import java.util.List;

import com.app.pojos.Job;
import com.app.exception.DuplicateRecordException;

public interface IJobService {

	public long add(Job pj) throws DuplicateRecordException;

	public void delete(Job pj);

	public Job findBypk(long pk);

	public Job findByName(String name);

	public void update(Job pj) throws DuplicateRecordException;

	public List<Job> list();

	public List<Job> list(int pageNo, int pageSize);

	public List<Job> search(Job pj);

	public List<Job> search(Job pj, int pageNo, int pageSize);




}
