package com.app.service;

import java.util.List;
import java.util.logging.Logger;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IApplicationDao;
import com.app.pojos.Application;
import com.app.exception.DuplicateRecordException;





@Service
public class ApplicationServiceImpl implements IApplicationService {

	private static Logger log=Logger.getLogger(ApplicationServiceImpl.class.getName());
	
	@Autowired
	private IApplicationDao dao;
	
	
	
	@Override
	@Transactional
	public long add(Application pj) throws DuplicateRecordException {
		log.info("ApplicationServiceImpl Add method start");
		long pk=dao.add(pj);
		log.info("ApplicationServiceImpl Add method end");
		return pk;
	}

	@Override
	@Transactional
	public void delete(Application pj) {
		log.info("ApplicationServiceImpl Delete method start");
		dao.delete(pj);
		log.info("ApplicationServiceImpl Delete method end");
		
	}

	@Override
	@Transactional
	public Application findBypk(long pk) {
		log.info("ApplicationServiceImpl findBypk method start");
		Application pj=dao.findBypk(pk);
		log.info("ApplicationServiceImpl findBypk method end");
		return pj;
	}

	@Override
	@Transactional
	public Application findByName(String name) {
		log.info("ApplicationServiceImpl findByApplicationName method start");
		Application pj=dao.findByName(name);
		log.info("ApplicationServiceImpl findByApplicationName method end");
		return pj;
	}
	
	@Override
	@Transactional
	public Application findByUserId(long id) {
		log.info("ApplicationServiceImpl findByApplicationName method start");
		Application pj=dao.findByUserId(id);
		log.info("ApplicationServiceImpl findByApplicationName method end");
		return pj;
	}

	@Override
	@Transactional
	public void update(Application pj) throws DuplicateRecordException {
		log.info("ApplicationServiceImpl update method start");
		dao.update(pj);
		log.info("ApplicationServiceImpl update method end");
	}

	@Override
	@Transactional
	public List<Application> list() {
		log.info("ApplicationServiceImpl list method start");
		List<Application> list=dao.list();
		log.info("ApplicationServiceImpl list method end");
		return list;
	}

	@Override
	@Transactional
	public List<Application> list(int pageNo, int pageSize) {
		log.info("ApplicationServiceImpl list method start");
		List<Application> list=dao.list(pageNo, pageSize);
		log.info("ApplicationServiceImpl list method end");
		return list;
	}

	@Override
	@Transactional
	public List<Application> search(Application pj) {
		log.info("ApplicationServiceImpl search method start");
		List<Application> list=dao.search(pj);
		log.info("ApplicationServiceImpl search method end");
		return list;
	}

	@Override
	@Transactional
	public List<Application> search(Application pj, int pageNo, int pageSize) {
		log.info("ApplicationServiceImpl search method start");
		List<Application> list=dao.search(pj, pageNo, pageSize);
		log.info("ApplicationServiceImpl search method end");
		return list;
	}

	
	
}
