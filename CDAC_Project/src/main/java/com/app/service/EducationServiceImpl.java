package com.app.service;

import java.util.List;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.app.dao.IEducationDao;
import com.app.exception.DuplicateRecordException;
import com.app.pojos.Education;





@Service
public class EducationServiceImpl implements IEducationService {

	private static Logger log=Logger.getLogger(EducationServiceImpl.class.getName());
	
	@Autowired
	private IEducationDao dao;
	
	
	
	@Override
	@Transactional
	public long add(Education pj) throws DuplicateRecordException {
		log.info("EducationServiceImpl Add method start");
		
		long pk=dao.add(pj);
		log.info("EducationServiceImpl Add method end");
		return pk;
	}

	@Override
	@Transactional
	public void delete(Education pj) {
		log.info("EducationServiceImpl Delete method start");
		dao.delete(pj);
		log.info("EducationServiceImpl Delete method end");
		
	}

	@Override
	@Transactional
	public Education findBypk(long pk) {
		log.info("EducationServiceImpl findBypk method start");
		Education pj=dao.findBypk(pk);
		log.info("EducationServiceImpl findBypk method end");
		return pj;
	}

	@Override
	@Transactional
	public Education findByName(String name) {
		log.info("EducationServiceImpl findByEducationName method start");
		Education pj=dao.findByName(name);
		log.info("EducationServiceImpl findByEducationName method end");
		return pj;
	}

	@Override
	@Transactional
	public void update(Education pj) throws DuplicateRecordException {
		log.info("EducationServiceImpl update method start");
		dao.update(pj);
		log.info("EducationServiceImpl update method end");
	}

	@Override
	@Transactional
	public List<Education> list() {
		log.info("EducationServiceImpl list method start");
		List<Education> list=dao.list();
		log.info("EducationServiceImpl list method end");
		return list;
	}

	@Override
	@Transactional
	public List<Education> list(int pageNo, int pageSize) {
		log.info("EducationServiceImpl list method start");
		List<Education> list=dao.list(pageNo, pageSize);
		log.info("EducationServiceImpl list method end");
		return list;
	}

	@Override
	@Transactional
	public List<Education> search(Education pj) {
		log.info("EducationServiceImpl search method start");
		List<Education> list=dao.search(pj);
		log.info("EducationServiceImpl search method end");
		return list;
	}

	@Override
	@Transactional
	public List<Education> search(Education pj, int pageNo, int pageSize) {
		log.info("EducationServiceImpl search method start");
		List<Education> list=dao.search(pj, pageNo, pageSize);
		log.info("EducationServiceImpl search method end");
		return list;
	}

	
	
}
