package com.app.pojos;


public interface DropdownList
{
	public String getKey();

	public String getValue();
}
