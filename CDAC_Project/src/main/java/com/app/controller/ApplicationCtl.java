package com.app.controller;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.app.pojos.Application;
import com.app.pojos.Education;
import com.app.pojos.Exprience;
import com.app.pojos.User;
import com.app.exception.DuplicateRecordException;
import com.app.form.ApplicationForm;
import com.app.form.ExprienceForm;
import com.app.service.IApplicationService;
import com.app.service.IEducationService;
import com.app.service.IExprienceService;
import com.app.util.DataUtility;
import com.app.util.DataValidator;

@Controller
@RequestMapping("/ctl/application")
public class ApplicationCtl extends BaseCtl {

	@Autowired
	private IApplicationService service;

	@Autowired
	private IEducationService educationService;

	@Autowired
	private IExprienceService exprienceService;

	private List<Education> eduList = null;

	private List<Exprience> expList = null;

	@GetMapping
	public String display(HttpSession session, @RequestParam(required = false) Long id,
			@ModelAttribute("form") ApplicationForm form, Model model) {
		if (form.getId() > 0) {
			Application bean = service.findBypk(id);
			form.populate(bean);
		}
		User upj = (User) session.getAttribute("user");
		Application pj = service.findByUserId(upj.getId());
		eduList = new ArrayList<Education>();
		expList = new ArrayList<Exprience>();
		eduList.add(new Education("", "", ""));
		form.setEduForm(eduList);
		form.setExpForm(expList);
		if (pj != null) {
			form.populate(pj);
			Education edupj = new Education();
			edupj.setApplicationId(pj.getId());
			form.setEduForm(educationService.search(edupj));
			Exprience exppj = new Exprience();
			exppj.setApplicationId(pj.getId());
			form.setExpForm(exprienceService.search(exppj));
		}
		return "application";
	}

	@PostMapping
	public String submit(HttpSession session, @Valid @ModelAttribute("form") ApplicationForm form,
			BindingResult bindingResult, Model model) {

		if (OP_RESET.equalsIgnoreCase(form.getOperation())) {
			return "redirect:/ctl/application";
		}

		if (OP_ADD_MORE.equalsIgnoreCase(form.getOperation())) {
			System.out.println("In Add More------------------------------");
			eduList = form.getEduForm();
			eduList.add(new Education("", "", ""));
			form.setEduForm(eduList);
			return "application";
		}
		if (OP_ADD_FIELD.equalsIgnoreCase(form.getOperation())) {
			System.out.println("In Add Fiels------------------------------");
			if (form.getExpForm() == null) {
				expList = new ArrayList<Exprience>();
			} else {
				expList = form.getExpForm();
			}
			expList.add(new Exprience("", "", ""));
			form.setExpForm(expList);
			return "application";
		}

		try {
			if (OP_SAVE.equalsIgnoreCase(form.getOperation())) {
				Application bean = (Application) form.getpj();
				if(!DataValidator.isName(bean.getDeclaration())) {
					model.addAttribute("jsCode",bean.getDeclaration());
					return "application";
				}
				
				/*
				 * if (bindingResult.hasErrors()) {
				 * System.out.println(bindingResult.getAllErrors()); return "application"; }
				 */
				
				System.out.println("In Save 1 -----------------");
				
				User upj = (User) session.getAttribute("user");
				bean.setUserId(upj.getId());
				if (bean.getId() > 0) {
					service.update(bean);
					Education epj = new Education();
					epj.setApplicationId(bean.getId());
					List edList = educationService.search(epj);
					Iterator<Education> edIt = edList.iterator();
					while (edIt.hasNext()) {
						Education educationpj = (Education) edIt.next();
						educationService.delete(educationpj);
					}
					List edList1 = form.getEduForm();
					Iterator<Education> edIt1 = edList1.iterator();
					while (edIt1.hasNext()) {
						Education edpj = (Education) edIt1.next();
						edpj.setApplicationId(bean.getId());
						educationService.add(edpj);
					}

					Exprience expj = new Exprience();
					expj.setApplicationId(bean.getId());
					List exList = exprienceService.search(expj);
					Iterator<Exprience> exIt = exList.iterator();
					while (exIt.hasNext()) {
						Exprience exppj = (Exprience) exIt.next();
						exprienceService.delete(exppj);
					}
					List exList1 = form.getExpForm();
					Iterator<Exprience> exIt1 = exList1.iterator();
					while (exIt1.hasNext()) {
						Exprience exppj = (Exprience) exIt1.next();
						exppj.setApplicationId(bean.getId());
						exprienceService.add(exppj);
					}

					model.addAttribute("success", "Application update Successfully!!!!");
				} else {
					System.out.println("In Save 2-----------------");
					long pk = service.add(bean);
					List edList = form.getEduForm();
					Iterator<Education> edIt = edList.iterator();
					while (edIt.hasNext()) {
						Education edpj = (Education) edIt.next();
						edpj.setApplicationId(pk);
						educationService.add(edpj);
					}
					List exList = form.getEduForm();
					if (exList != null) {
						Iterator<Exprience> exIt = exList.iterator();
						while (edIt.hasNext()) {
							Exprience expj = (Exprience) exIt.next();
							expj.setApplicationId(pk);
							exprienceService.add(expj);
						}
					}
					System.out.println("In Save 3 -----------------");
					model.addAttribute("success", "Application Added Successfully!!!!");
				}
				return "application";
			}
		} catch (DuplicateRecordException e) {
			model.addAttribute("error", e.getMessage());
			return "application";
		}
		return "";
	}

	@RequestMapping(value = "/search", method = { RequestMethod.GET, RequestMethod.POST })
	public String searchList(@ModelAttribute("form") ApplicationForm form,
			@RequestParam(required = false) String operation, HttpSession session, Model model) {

		if (OP_RESET.equalsIgnoreCase(operation)) {
			return "redirect:/ctl/application/search";
		}

		int pageNo = form.getPageNo();
		int pageSize = form.getPageSize();

		if (OP_NEXT.equals(operation)) {
			pageNo++;
		} else if (OP_PREVIOUS.equals(operation)) {
			pageNo--;
		} else if (OP_NEW.equals(operation)) {
			return "redirect:/ctl/application";
		}

		pageNo = (pageNo < 1) ? 1 : pageNo;
		pageSize = (pageSize < 1) ? 10 : pageSize;

		if (OP_DELETE.equals(operation)) {
			pageNo = 1;
			if (form.getIds() != null) {
				for (long id : form.getIds()) {
					Application pj = new Application();
					pj.setId(id);
					service.delete(pj);
				}
				model.addAttribute("success", "Deleted Successfully!!!");
			} else {
				model.addAttribute("error", "Select at least one record");
			}
		}
		Application pj = (Application) form.getpj();
		List<Application> list = service.search(pj, pageNo, pageSize);
		List<Application> totallist = service.search(pj);
		model.addAttribute("list", list);

		if (list.size() == 0 && !OP_DELETE.equalsIgnoreCase(operation)) {
			model.addAttribute("error", "Record not found");
		}

		int listsize = list.size();
		int total = totallist.size();
		int pageNoPageSize = pageNo * pageSize;

		form.setPageNo(pageNo);
		form.setPageSize(pageSize);
		model.addAttribute("pageNo", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("listsize", listsize);
		model.addAttribute("total", total);
		model.addAttribute("pagenosize", pageNoPageSize);
		model.addAttribute("form", form);
		return "applicationList";
	}

}
