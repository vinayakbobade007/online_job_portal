package com.app.controller;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.tomcat.util.http.fileupload.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.app.pojos.ApplyJob;
import com.app.pojos.Job;
import com.app.pojos.User;
import com.app.exception.DuplicateRecordException;
import com.app.form.ApplyJobForm;
import com.app.service.IApplyJobService;
import com.app.service.IJobService;
import com.app.util.DataUtility;

@Controller
@RequestMapping("/ctl/apply")
public class ApplyJobCtl extends BaseCtl {

	@Autowired
	private IApplyJobService service;

	@Autowired
	private IJobService jobService;

	@GetMapping
	public String display(HttpSession session, @RequestParam(required = false) Long id, Long jId,
			@ModelAttribute("form") ApplyJobForm form, Model model) {
		if (form.getId() > 0) {
			ApplyJob bean = service.findBypk(id);
			form.populate(bean);
		}
		if (DataUtility.getLong(String.valueOf(jId)) > 0) {
			form.setJob(jobService.findBypk(jId));
			session.setAttribute("jobId", jId);
		}
		return "apply";
	}

	@PostMapping
	public String submit(@RequestParam("file") MultipartFile file,HttpSession session, @Valid @ModelAttribute("form") ApplyJobForm form,
			BindingResult bindingResult, Model model) {

		if (OP_RESET.equalsIgnoreCase(form.getOperation())) {
			return "redirect:/ctl/apply";
		}

		try {
			if (OP_SUBMIT.equalsIgnoreCase(form.getOperation())) {

				if (bindingResult.hasErrors()) {
					return "apply";
				}

				ApplyJob bean = (ApplyJob) form.getpj();
				long jId = DataUtility.getLong(String.valueOf(session.getAttribute("jobId")));
				Job jpj = jobService.findBypk(jId);
				bean.setJobId(jpj.getId());
				bean.setJobName(jpj.getName());
				bean.setCompanyName(jpj.getCompanyName());
				bean.setRecruiterId(jpj.getRecruiterId());
				User upj=(User)session.getAttribute("user");
				bean.setUserId(upj.getId());
				bean.setUserName(upj.getFirstName()+" "+upj.getLastName());
				bean.setApDate(new Date());
				bean.setResumeFile(file.getBytes());
				bean.setFileName(DataUtility.removeSpace(upj.getFirstName()+upj.getEmail())+".pdf");
				if (bean.getId() > 0) {
					service.update(bean);
					model.addAttribute("success", "Job Applyed update Successfully!!!!");
				} else {
					service.add(bean);
					model.addAttribute("success", "Job Applyed Successfully!!!!");
				}
				return "apply";
			}
		} catch (DuplicateRecordException e) {
			model.addAttribute("error", e.getMessage());
			return "apply";
		} catch (IOException e) {
			e.printStackTrace();
		}
		return "";
	}

	@RequestMapping(value = "/search", method = { RequestMethod.GET, RequestMethod.POST })
	public String searchList(@ModelAttribute("form") ApplyJobForm form,
			@RequestParam(required = false) String operation, HttpSession session, Model model) {

		if (OP_RESET.equalsIgnoreCase(operation)) {
			return "redirect:/ctl/apply/search";
		}

		int pageNo = form.getPageNo();
		int pageSize = form.getPageSize();

		if (OP_NEXT.equals(operation)) {
			pageNo++;
		} else if (OP_PREVIOUS.equals(operation)) {
			pageNo--;
		} else if (OP_NEW.equals(operation)) {
			return "redirect:/ctl/apply";
		}

		pageNo = (pageNo < 1) ? 1 : pageNo;
		pageSize = (pageSize < 1) ? 10 : pageSize;

		if (OP_DELETE.equals(operation)) {
			pageNo = 1;
			if (form.getIds() != null) {
				for (long id : form.getIds()) {
					ApplyJob pj = new ApplyJob();
					pj.setId(id);
					service.delete(pj);
				}
				model.addAttribute("success", "Deleted Successfully!!!");
			} else {
				model.addAttribute("error", "Select at least one record");
			}
		}
		ApplyJob pj = (ApplyJob) form.getpj();
		User upj=(User)session.getAttribute("user");
		if(upj.getRoleId()==2) {
			pj.setRecruiterId(upj.getId());
		}if(upj.getRoleId()==3) {
			pj.setUserId(upj.getId());
		}
		
		if(pj.getFileName()!=null) {
			try {
				DataUtility.cmdCommand(pj.getFileName().trim());
			} catch (IOException e) {
				e.printStackTrace();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		List<ApplyJob> list = service.search(pj, pageNo, pageSize);
		List<ApplyJob> totallist = service.search(pj);
		model.addAttribute("list", list);

		if (list.size() == 0 && !OP_DELETE.equalsIgnoreCase(operation)) {
			model.addAttribute("error", "Record not found");
		}

		int listsize = list.size();
		int total = totallist.size();
		int pageNoPageSize = pageNo * pageSize;

		form.setPageNo(pageNo);
		form.setPageSize(pageSize);
		model.addAttribute("pageNo", pageNo);
		model.addAttribute("pageSize", pageSize);
		model.addAttribute("listsize", listsize);
		model.addAttribute("total", total);
		model.addAttribute("pagenosize", pageNoPageSize);
		model.addAttribute("form", form);
		return "applyList";
	}
	
	@GetMapping("/getFile/{id}")
	public void getStudentPhoto(HttpServletResponse response, @PathVariable("id") long id) throws Exception {
		response.setContentType("application/pdf");

		Blob blb=service.getFileById(id);
		
		byte[] bytes = blb.getBytes(1, (int) blb.length());
		InputStream inputStream = new ByteArrayInputStream(bytes);
		IOUtils.copy(inputStream, response.getOutputStream());
	}

}
