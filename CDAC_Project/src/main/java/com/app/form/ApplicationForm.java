package com.app.form;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotEmpty;

import com.app.pojos.Application;
import com.app.pojos.Base;
import com.app.pojos.Education;
import com.app.pojos.Exprience;
import com.app.util.DataUtility;

public class ApplicationForm extends BaseForm {

	@NotEmpty(message = "Name is required")
	private String name;

	@NotEmpty(message = "Email is required")
	private String email;

	@NotEmpty(message = "Mobile No is required")
	private String mobile;
	
	@NotEmpty(message = "Skill is required")
	private String skill;

	@NotEmpty(message = "Hobbies is required")
	private String hobbies;

	@NotEmpty(message = "Personal Detail is required")
	private String pDetail;

	@NotEmpty(message = "Declaration is required")
	private String declaration;

	@NotEmpty(message = "Date is required")
	private String date;

	@NotEmpty(message = "Place is required")
	private String place;

	@NotEmpty(message = "Objective is required")
	private String objective;

	@NotEmpty(message = "Past Experience is required")
	private String pastExprience;
	
	private List<Education> eduForm;
	
	private List<Exprience> expForm;
	
	
	
	

	public List<Exprience> getExpForm() {
		return expForm;
	}

	public void setExpForm(List<Exprience> expForm) {
		this.expForm = expForm;
	}

	public List<Education> getEduForm() {
		return eduForm;
	}

	public void setEduForm(List<Education> eduForm) {
		this.eduForm = eduForm;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getSkill() {
		return skill;
	}

	public void setSkill(String skill) {
		this.skill = skill;
	}

	public String getHobbies() {
		return hobbies;
	}

	public void setHobbies(String hobbies) {
		this.hobbies = hobbies;
	}

	public String getpDetail() {
		return pDetail;
	}

	public void setpDetail(String pDetail) {
		this.pDetail = pDetail;
	}

	public String getDeclaration() {
		return declaration;
	}

	public void setDeclaration(String declaration) {
		this.declaration = declaration;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getObjective() {
		return objective;
	}

	public void setObjective(String objective) {
		this.objective = objective;
	}

	public String getPastExprience() {
		return pastExprience;
	}

	public void setPastExprience(String pastExprience) {
		this.pastExprience = pastExprience;
	}

	@Override
	public Base getpj() {
		Application pj = new Application();
		pj.setId(id);
		pj.setName(name);
		pj.setEmail(email);
		pj.setMobile(mobile);
		pj.setSkill(skill);
		pj.setHobbies(hobbies);
		pj.setpDetail(pDetail);
		pj.setDeclaration(declaration);
		pj.setDate(DataUtility.getDate(date));
		pj.setPlace(place);
		pj.setObjective(objective);
		pj.setPastExprience(pastExprience);
		pj.setCreatedBy(createdBy);
		pj.setModifiedBy(modifiedBy);
		pj.setCreatedDatetime(createdDateTime);
		pj.setModifiedDatetime(modifiedDateTime);
		return pj;
	}

	@Override
	public void populate(Base bpj) {
		Application pj = (Application) bpj;
		id=pj.getId();
		name = pj.getName();
		email = pj.getEmail();
		mobile = pj.getMobile();
		skill = pj.getEmail();
		hobbies = pj.getHobbies();
		pDetail = pj.getpDetail();
		declaration = pj.getDeclaration();
		date = DataUtility.getDateString(pj.getDate());
		place = pj.getPlace();
		objective = pj.getObjective();
		pastExprience = pj.getPastExprience();
		createdBy = pj.getCreatedBy();
		modifiedBy = pj.getModifiedBy();
		createdDateTime = pj.getCreatedDatetime();
		modifiedDateTime = pj.getModifiedDatetime();

	}

}
