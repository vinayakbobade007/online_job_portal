package com.app.form;

import javax.validation.constraints.NotEmpty;

import com.app.pojos. *;


public abstract class ForgetPasswordForm extends BaseForm {

	@NotEmpty(message = "Login is required")
	private String login;

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public Base getpj() {

		User pj = new User();
		pj.setLogin(login);
		return pj;
	}

	@Override
	public void populate(Base bpj) {

	}

}
