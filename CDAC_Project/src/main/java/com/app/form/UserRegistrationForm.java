package com.app.form;

import javax.validation.constraints.NotEmpty;

import org.springframework.web.multipart.MultipartFile;

import com.app.pojos.Base;
import com.app.pojos.User;
import com.app.util.DataUtility;

public class UserRegistrationForm extends BaseForm {

	@NotEmpty(message = "First Name is required")
	private String firstName;

	@NotEmpty(message = "LastName is required")
	private String lastName;

	@NotEmpty(message = "Login is required")
	private String login;

	@NotEmpty(message = "Password is required")
	private String password;

	@NotEmpty(message = "Mobile No is required")
	private String mobileNo;

	@NotEmpty(message = "Email is required")
	private String email;

	@NotEmpty(message = "Confirm Password is required")
	private String confirmPassword;

	@NotEmpty(message = "gender is required")
	private String gender;

	@NotEmpty(message = "Date of Birth is required")
	private String dob;
	
	private MultipartFile profilePic;
	
	

	public MultipartFile getProfilePic() {
		return profilePic;
	}

	public void setProfilePic(MultipartFile profilePic) {
		this.profilePic = profilePic;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	@Override
	public Base getpj() {
		User pj = new User();
		pj.setId(id);
		pj.setFirstName(firstName);
		pj.setLastName(lastName);
		pj.setLogin(login);
		pj.setPassword(password);
		pj.setMobileNo(mobileNo);
		pj.setDob(DataUtility.getDate(dob));
		pj.setGender(gender);
		pj.setEmail(email);
		pj.setCreatedBy(createdBy);
		pj.setModifiedBy(modifiedBy);
		pj.setCreatedDatetime(createdDateTime);
		pj.setModifiedDatetime(modifiedDateTime);
		return pj;
	}

	@Override
	public void populate(Base bpj) {
		User pj = (User) bpj;
		id = pj.getId();
		firstName = pj.getFirstName();
		lastName = pj.getLastName();
		login = pj.getLogin();
		password = pj.getPassword();
		mobileNo = pj.getMobileNo();
		gender = pj.getGender();
		email = pj.getEmail();
		dob = DataUtility.getDateString(pj.getDob());
		createdBy = pj.getCreatedBy();
		modifiedBy = pj.getModifiedBy();
		createdDateTime = pj.getCreatedDatetime();
		modifiedDateTime = pj.getModifiedDatetime();
	}

}
