package com.app.form;

import java.util.Date;

import org.springframework.web.multipart.MultipartFile;

import com.app.pojos.ApplyJob;
import com.app.pojos.Base;
import com.app.pojos.Job;

public class ApplyJobForm extends BaseForm {

	private long jobId;
	private String jobName;
	private String companyName;
	private long userId;
	private String userName;
	private long recruiterId;
	private String recruiterName;
	private Date apDate;
	
	private MultipartFile file;
	
	
	private String fileName;
	

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	private Job jobpj;
	
	public Job getJobpj() {
		return jobpj;
	}

	public void setJob(Job jobpj) {
		this.jobpj = jobpj;
	}

	public long getJobId() {
		return jobId;
	}

	public void setJobId(long jobId) {
		this.jobId = jobId;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public long getRecruiterId() {
		return recruiterId;
	}

	public void setRecruiterId(long recruiterId) {
		this.recruiterId = recruiterId;
	}

	public String getRecruiterName() {
		return recruiterName;
	}

	public void setRecruiterName(String recruiterName) {
		this.recruiterName = recruiterName;
	}

	public Date getApDate() {
		return apDate;
	}

	public void setApDate(Date apDate) {
		this.apDate = apDate;
	}

	@Override
	public Base getpj() {
		ApplyJob pj=new ApplyJob();
		pj.setJobId(jobId);
		pj.setJobName(jobName);
		pj.setCompanyName(companyName);
		pj.setUserId(userId);
		pj.setUserName(userName);
		pj.setRecruiterId(recruiterId);
		pj.setRecruiterName(recruiterName);
		pj.setApDate(apDate);
		pj.setFileName(fileName);
		pj.setCreatedBy(createdBy);
		pj.setModifiedBy(modifiedBy);
		pj.setCreatedDatetime(createdDateTime);
		pj.setModifiedDatetime(modifiedDateTime);
		return pj;
	}

	@Override
	public void populate(Base bpj) {
		ApplyJob pj=(ApplyJob)bpj;
		jobId=pj.getJobId();
		jobName=pj.getJobName();
		companyName=pj.getCompanyName();
		userId=pj.getUserId();
		userName=pj.getUserName();
		recruiterId=pj.getRecruiterId();
		recruiterName=pj.getRecruiterName();
		fileName=pj.getFileName();
		apDate=pj.getApDate();
		createdBy = pj.getCreatedBy();
		modifiedBy = pj.getModifiedBy();
		createdDateTime = pj.getCreatedDatetime();
		modifiedDateTime = pj.getModifiedDatetime();
	}

}
