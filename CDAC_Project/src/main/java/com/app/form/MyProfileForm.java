package com.app.form;

import javax.validation.constraints.NotEmpty;

import com.app.pojos.Base;
import com.app.pojos.User;
import com.app.util.DataUtility;

public class MyProfileForm extends BaseForm {

	@NotEmpty(message = "First Name is required")
	private String firstName;
	
	@NotEmpty(message = "LastName is required")
	private String lastName;
	
	@NotEmpty(message = "Login is required")
	private String login;
	
	@NotEmpty(message = "Mobile No is required")
	private String mobileNo;

	@NotEmpty(message = "Email is required")
	private String email;
	
	@NotEmpty(message = "gender is required")
	private String gender;
	
	@NotEmpty(message = "Date of Birth is required")
	private String dob;

	
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getDob() {
		return dob;
	}

	public void setDob(String dob) {
		this.dob = dob;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getMobileNo() {
		return mobileNo;
	}

	public void setMobileNo(String mobileNo) {
		this.mobileNo = mobileNo;
	}

	public Base getpj() {
		User pj = new User();
		pj.setMobileNo(mobileNo);
		pj.setLogin(login);
		pj.setFirstName(firstName);
		pj.setLastName(lastName);
		pj.setDob(DataUtility.getDate(dob));
		pj.setGender(gender);
		return pj;
	}

	
	public void populate(Base bpj) {
		User pj = (User) bpj;
		mobileNo = pj.getMobileNo();
		login = pj.getLogin();
		firstName = pj.getFirstName();
		lastName = pj.getLastName();
		gender=pj.getGender();
		dob=DataUtility.getDateString(pj.getDob());

	}

	

}
