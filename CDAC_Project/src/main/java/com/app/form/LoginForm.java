package com.app.form;

import javax.validation.constraints.NotEmpty;

import com.app.pojos. *;





public class LoginForm extends BaseForm {

	@NotEmpty(message = "Login is required")
	private String login;
	@NotEmpty(message = "Password is required")
	private String password;


	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Base getDto() {
		User dto=new User();
		dto.setLogin(login);
		dto.setPassword(password);
		return dto;
	}

	@Override
	public void populate(Base bpj) {
		User pj=(User) bpj;
		login=pj.getLogin();
		password=pj.getPassword();
	}

	@Override
	public Base getpj() {
		// TODO Auto-generated method stub
		return null;
	}

}
