package com.app.form;

import com.app.pojos.Base;
import com.app.pojos.Education;

public class EducationForm extends BaseForm {

	private String courseName;
	private String insName;
	private String percentage;
	
	public EducationForm() {
	}
	
	public EducationForm(String courseName, String insName, String percentage) {
		this.courseName = courseName;
		this.insName = insName;
		this.percentage = percentage;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getInsName() {
		return insName;
	}

	public void setInsName(String insName) {
		this.insName = insName;
	}

	public String getPercentage() {
		return percentage;
	}

	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}

	@Override
	public Base getpj() {
		Education pj=new Education();
		pj.setCourseName(courseName);
		pj.setInsName(insName);
		pj.setPercentage(percentage);
		return pj;
	}

	@Override
	public void populate(Base bpj) {
		Education pj=(Education)bpj;
		courseName=pj.getCourseName();
		insName=pj.getInsName();
		percentage=pj.getPercentage();
	}

}
