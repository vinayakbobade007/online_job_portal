package com.app.form;

import com.app.pojos.Base;
import com.app.pojos.Exprience;

public class ExprienceForm extends BaseForm {

	private String companyName;
	private String duration;
	private String technology;

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getTechnology() {
		return technology;
	}

	public void setTechnology(String technology) {
		this.technology = technology;
	}

	@Override
	public Base getpj() {
		Exprience pj = new Exprience();
		pj.setCompanyName(companyName);
		pj.setDuration(duration);
		pj.setTechnology(technology);
		return pj;
	}

	@Override
	public void populate(Base bpj) {
		Exprience pj = (Exprience) bpj;
		companyName = pj.getCompanyName();
		duration = pj.getDuration();
		technology = pj.getTechnology();
	}

}
